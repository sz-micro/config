<?php

namespace GranitSDK;


use GranitSDK\Config\MemCached;
use GranitSDK\Config\MemCachedSession;
use GranitSDK\Config\MySQL;


class Config
{
	protected static $instances = [];

	private static function getInstance($class)
	{
		if (empty(self::$instances[$class])) {
			self::$instances[$class] = new $class();
		}

		return self::$instances[$class];
	}

	public static function get():self
	{
		return self::getInstance(self::class);
	}

	public function getMemCached():MemCached
	{
		return self::getInstance(MemCached::class);
	}
	public function getMemCachedSession():MemCachedSession
	{
		return self::getInstance(MemCachedSession::class);
	}

	public function getMySQL():MySQL
	{
		return self::getInstance(MySQL::class);
	}
}