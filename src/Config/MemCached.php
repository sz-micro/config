<?php

namespace GranitSDK\Config;

class MemCached extends AbstractConfig
{
	const HOST = 'MEMCACHED_HOST';
	const LIFETIME = 'MEMCACHED_LIFETIME';

	public function getHosts()
	{
		$hostsString = $this->env(self::HOST);
		$hosts = explode(',', $hostsString);

		$config = [];
		foreach ($hosts as $hostAndPort) {

			$parts = explode(':', $hostAndPort);

			$host = $parts[0] ?? null;
			$port = $parts[1] ?? null;
			$weight = $parts[2] ?? 1;

			$config[] = [
				'host' => $host,
				'port' => $port,
				'weight' => $weight ?? 1,
			];
		}

		return $config;
	}

	public function getLifeTime()
	{
		return $this->env(self::LIFETIME);
	}
}