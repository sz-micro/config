<?php

namespace GranitSDK\Config\Exception;

use Throwable;

class NotFound extends \Exception
{
	public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
	{
		parent::__construct('ENV var not found ['. $message .']', $code, $previous);
	}

}