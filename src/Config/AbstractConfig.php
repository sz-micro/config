<?php

namespace GranitSDK\Config;


use GranitSDK\Config\Exception\NotFound;

abstract class AbstractConfig
{
	const DEFAULTS = [
		MemCached::HOST => 'memcached:11211',
		MemCached::LIFETIME => '3600',
		MemCachedSession::HOST => 'memcached-session-1:11211,memcached-session-2:11211',
		MemCachedSession::LIFETIME => '3600',
	];

	public function env($name)
	{
		$variable = getenv($name);

		if (empty($variable)) {
			if (isset(self::DEFAULTS[$name])) {
				$variable = self::DEFAULTS[$name];
			}
			else {
				throw new NotFound($name);
			}
		}

		return $variable;
	}
}