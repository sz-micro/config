<?php

namespace GranitSDK\Config;

class MySQL extends AbstractConfig
{
	const HOST = 'MYSQL_HOST';
	const USER = 'MYSQL_USER';
	const PASS = 'MYSQL_PASS';
	const DB   = 'MYSQL_DB';

	public function getHost()
	{
		return $this->env(self::HOST);
	}

	public function getUser()
	{
		return $this->env(self::USER);
	}

	public function getPass()
	{
		return $this->env(self::PASS);
	}

	public function getDb()
	{
		return $this->env(self::DB);
	}

}